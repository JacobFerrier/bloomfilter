package bloomfilter

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"math"
	"os"
	"strconv"
	"strings"

	"bitbucket.org/JacobFerrier/racco/functions"
	"github.com/spaolacci/murmur3"
)

////
//
// Filter methods
//
////

// Filter object acts as the bloom filter
type Filter struct {
	bitmap           map[int]uint8
	numItemsInFilter int
	fpr              float64
	numBitsInFilter  int
	numHashFunctions int
}

// newFilter acts as the constructor for Filter, given a number of items in the filter and false positive rate
func newFilter(numItemsInFilter int, fpr float64) Filter {
	b := make(map[int]uint8)
	numBitsInFilter := getNumBitsInFilter(numItemsInFilter, fpr)
	numHashFunctions := getNumHashFunctions(numBitsInFilter, numItemsInFilter)

	for i := 0; i < numBitsInFilter; i++ {
		b[i] = 0
	}

	return Filter{b, numItemsInFilter, fpr, numBitsInFilter, numHashFunctions}
}

// getNumBitsInFilter calculates the appropriate number of bits for a filter
func getNumBitsInFilter(numItemsInFilter int, fpr float64) int {
	return int(math.Ceil((-1.0 * float64(numItemsInFilter) * math.Log(fpr)) / (math.Pow(math.Log(2), 2))))
}

// getNumHashFunctions calculates the appropriate number of hash functions for a filter
func getNumHashFunctions(numBitsInFilter int, numItemsInFilter int) int {
	return int(math.Round(float64(numBitsInFilter/numItemsInFilter) * math.Log(2)))
}

// fillFitler with a passed in Dataset object
func (f *Filter) fillFilter(d Dataset) {
	for _, v := range d.getElements() {
		f.add(v)
	}
}

// add a passed in string into the Filter object
func (f *Filter) add(s string) {
	h1, h2 := murmur3.Sum128([]byte(s))
	for i := 0; i < f.numHashFunctions; i++ {
		h1 += uint64(f.numHashFunctions) * h2
		f.bitmap[int(h1%uint64(f.numBitsInFilter))] = 1
	}
}

// probablyExists tests if a string passed in probably exists in the filter
func (f *Filter) probablyExists(s string) bool {
	returnVal := true
	h1, h2 := murmur3.Sum128([]byte(s))
	for i := 0; i < f.numHashFunctions; i++ {
		h1 += uint64(f.numHashFunctions) * h2
		if f.bitmap[int(h1%uint64(f.numBitsInFilter))] == 0 {
			returnVal = false
		}
	}

	return returnVal
}

// parseFilter takes the filename of a filter and parses it in as a Filter object
func parseFilter(filename string) Filter {
	contents, err := ioutil.ReadFile(filename)
	functions.Check(err)

	numItemsInFilter, err := strconv.Atoi(strings.Split(string(bytes.Split(contents, []byte("\n"))[0]), ": ")[1])
	functions.Check(err)
	fpr, err := strconv.ParseFloat(strings.Split(string(bytes.Split(contents, []byte("\n"))[1]), ": ")[1], 64)
	functions.Check(err)
	numBitsInFilter, err := strconv.Atoi(strings.Split(string(bytes.Split(contents, []byte("\n"))[2]), ": ")[1])
	functions.Check(err)
	numHashFunctions, err := strconv.Atoi(strings.Split(string(bytes.Split(contents, []byte("\n"))[3]), ": ")[1])
	functions.Check(err)
	bitString := string(bytes.Split(contents, []byte("\n"))[5])

	b := make(map[int]uint8)

	for i := 0; i < len(bitString); i++ {
		b[i] = uint8(functions.Inter(strconv.ParseUint(string(bitString[i]), 10, 8))[0].(uint64))
	}

	return Filter{b, numItemsInFilter, fpr, numBitsInFilter, numHashFunctions}
}

////
//
// Comparison methods
//
////

// compareToSet compares a Filter object to the passed in Dataset oject
func (f *Filter) compareToDataset(ds Dataset) float32 {
	var hits int
	for _, element := range ds.getElements() {
		if f.probablyExists(element) {
			hits++
		}
	}

	fmt.Println(strconv.Itoa(hits) + " / " + strconv.Itoa(ds.size()))

	return float32(hits) / float32(ds.size())
}

////
//
// Dataset methods
//
////

// parseDataset gets the contents of a dataset and returns a byte slice
func parseDataset(filename string) []byte {
	contents, err := ioutil.ReadFile(filename)
	functions.Check(err)

	out := []byte{}
	for _, v := range bytes.Split(contents, []byte("\n")) {
		if len(v) > 0 {
			out = append(out, bytes.Split(v, []byte("\t"))[0]...)
			out = append(out, []byte(" ")...)
		}
	}

	return out
}

// Dataset object
type Dataset struct {
	elements map[string]struct{}
}

// newDataset acts as the constructor for Dataset, creates a dataset from a file given filename
func newDataset(filename string) Dataset {
	set := Dataset{make(map[string]struct{})}
	for _, v := range bytes.Split(parseDataset(filename), []byte(" ")) {
		if len(v) > 0 {
			set.addElement(string(v))
		}
	}

	return set
}

// addElement adds a string element into a Dataset object's elements
func (d *Dataset) addElement(s string) {
	d.elements[s] = struct{}{}
}

// getElements returns the elements in a Dataset object as a string slice
func (d *Dataset) getElements() []string {
	keys := []string{}
	for k := range d.elements {
		keys = append(keys, k)
	}

	return keys
}

// size returns the number of elements in a Dataset object
func (d *Dataset) size() int {
	return len(d.elements)
}

////
//
// Operator methods
//
////

// createProfile generates a bloomfilter profile given a file
func createProfile(destinationDir string, datasetsDir string, file os.FileInfo, fpr float64) {
	myFilter := CreateAndFillFilter(datasetsDir+file.Name(), fpr)
	out := ("#number of items in filter: " + strconv.Itoa(myFilter.numItemsInFilter)) + "\n" +
		("#false positive rate of filter: " + fmt.Sprintf("%g", myFilter.fpr)) + "\n" +
		("#number of bits in filter: " + strconv.Itoa(myFilter.numBitsInFilter)) + "\n" +
		("#number of hash functions for filter: " + strconv.Itoa(myFilter.numHashFunctions)) + "\n" +
		("#bitmap:") + "\n"

	for i := 0; i < myFilter.numBitsInFilter; i++ {
		out += strconv.FormatUint(uint64(myFilter.bitmap[i]), 10)
	}

	err := ioutil.WriteFile(destinationDir+strings.Split(file.Name(), ".")[0]+".profile", []byte(out), 0644)
	functions.Check(err)
}

// CreateAndFillFilter creates a new bloom filter from a file given a filename and a desired flase positive rate (fpr), the contents of the input file determine the number of
// items in the filter and the fpr is supplied by the user here, the number of bits in the filter as well as the number of hash functions are calculated automatically
func CreateAndFillFilter(filename string, fpr float64) Filter {
	inputSet := newDataset(filename)
	myFilter := newFilter(inputSet.size(), fpr)
	myFilter.fillFilter(inputSet)

	return myFilter
}

// ProfileDatasets creates profiles for a set of datasets provided, also takes in a false positive rate (fpr) used for bloom filter design
func ProfileDatasets(destinationDir string, datasetsDir string, fpr float64) {
	files, err := ioutil.ReadDir(datasetsDir)
	functions.Check(err)

	for _, file := range files {
		if !strings.HasPrefix(file.Name(), ".") {
			fmt.Println("Creating " + destinationDir + strings.Split(file.Name(), ".")[0] + ".profile")
			createProfile(destinationDir, datasetsDir, file, fpr)
		}
	}
}

// FindFilterMatch takes in one filter profile as a filename and finds the best match for that filter profile against other datasets
func FindFilterMatch(filename string, datasets string) {
	myFilter := parseFilter(filename)

	files, err := ioutil.ReadDir(datasets)
	functions.Check(err)

	scoresTotal := float32(0)
	numScores := 0
	maxScore := float32(-1)
	minScore := float32(2)

	for _, file := range files {
		if !strings.HasPrefix(file.Name(), ".") {
			fmt.Println("Comparing filter against " + file.Name())
			score := myFilter.compareToDataset(newDataset(datasets + file.Name()))
			fmt.Println(score)
			if score != float32(1) { //the trivial perfect score is not included in any of these counters
				scoresTotal += score
				numScores++
				if score > maxScore {
					maxScore = score
				}
				if score < minScore {
					minScore = score
				}
			}
		}
	}

	fmt.Println("Mean score (excluding trivial perfect score): " + fmt.Sprintf("%f", scoresTotal/float32(numScores)))
	fmt.Println("Maximum score (excluding trivial perfect score): " + fmt.Sprintf("%f", maxScore))
	fmt.Println("Minimum score (excluding trivial perfect score): " + fmt.Sprintf("%f", minScore))
}
